----------------------------------------------------------------------------------------------------
-- SeasonsSnowHeightChangedEvent
----------------------------------------------------------------------------------------------------
-- Purpose:  Sends new actual snow height to clients
--
-- Copyright (c) Realismus Modding, 2020
----------------------------------------------------------------------------------------------------

SeasonsSnowHeightChangedEvent = {}
local SeasonsSnowHeightChangedEvent_mt = Class(SeasonsSnowHeightChangedEvent, Event)

InitEventClass(SeasonsSnowHeightChangedEvent, "SeasonsSnowHeightChangedEvent")

function SeasonsSnowHeightChangedEvent:emptyNew()
    return Event:new(SeasonsSnowHeightChangedEvent_mt)
end

function SeasonsSnowHeightChangedEvent:new(height)
    local self = SeasonsSnowHeightChangedEvent:emptyNew()

    self.height = math.max(height, 0)

    return self
end

function SeasonsSnowHeightChangedEvent:writeStream(streamId, connection)
    streamWriteUInt8(streamId, self.height / SeasonsSnowHandler.LAYER_HEIGHT)
end

function SeasonsSnowHeightChangedEvent:readStream(streamId, connection)
    self.height = streamReadUInt8(streamId) * SeasonsSnowHandler.LAYER_HEIGHT

    self:run(connection)
end

function SeasonsSnowHeightChangedEvent:run(connection)
    if connection:getIsServer() then
        -- on client
        g_seasons.snowHandler.height = self.height
        g_seasons.snowHandler:onHeightChanged()
    end
end
